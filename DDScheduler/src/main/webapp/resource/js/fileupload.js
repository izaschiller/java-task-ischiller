/***** jQuery to prevent form submission if no file is selected ****/
$(document).ready(function() {        	 
	$("#fileUploadErr").hide();

	/***** Hide error message ****/
	$('#fileAttachment').click(function(eObj) {
		$("#fileUploadErr").hide();
	});

	/***** Check whether attachment is uploaded ****/
	$('#uploadBtn').click(function(eObj) {
		var file = $("#fileAttachment").map(function() {
			return $(this).val().trim() ? true : false;
		}).get();
		if (file.includes(true)) {
			/***** Do Nothing ****/
		} else {
			$("#fileUploadErr").css({'color':'red', 'font-weight': 'bold'}).show();
			eObj.preventDefault();
		}
	});
});