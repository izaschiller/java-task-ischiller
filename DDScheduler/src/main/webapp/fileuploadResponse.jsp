<%@page import="java.util.List"%>
<%@page import="com.dd.domain.UploadDetail"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Deloitte Digital Away Day Scheduler</title>

<link rel="stylesheet" href="resource/css/main.css" />
</head>
<body>
	<div class="panel">
		<b>File Upload Status</b>
		<table class="bordered_table">
			<tbody>
				<% List<UploadDetail> uploadDetails = (List<UploadDetail>)request.getAttribute("uploadedFiles");
		    			for(int i=0; i<uploadDetails.size(); i++) {
		    	    %>
				<tr>
					<td align="center"><span id="fileName"><%=uploadDetails.get(i).getFileName() %></span></td>
					<td align="center"><span id="fileSize"><%=uploadDetails.get(i).getFileSize() %>
							KB</span></td>
					<td align="center"><span id="fileuploadStatus"><%=uploadDetails.get(i).getUploadStatus() %></span></td>
				</tr>
				<% } %>

			</tbody>
		</table>
        </br>
        </br>
        
        <b>OUTPUT</b>
        </br>
		<% List<String> activityList = (List<String>)request.getAttribute("activityList");
		    			for(String s:activityList) {%>
		<tr>
			<td><%=s%></td>
			</br>
		</tr>
		<%
            };
        %>
		<div class="margin_top_15px">
			<a id="fileUpload" class="hyperLink"
				href="<%=request.getContextPath()%>/fileupload.jsp">Back</a>
		</div>
	</div>
</body>
</html>