package com.dd.domain;

public class Activity{
private String activityName;
private int duration;
private boolean added;
public boolean isAdded() {
	return added;
}
public void setAdded(boolean added) {
	this.added = added;
}
public String getActivityName() {
	return activityName;
}
public void setActivityName(String activityName) {
	this.activityName = activityName;
}
public int getDuration() {
	return duration;
}
public void setDuration(int duration) {
	this.duration = duration;
}
}
