package com.dd.servlet;

import java.io.BufferedReader;
import java.io.File;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.dd.constants.DDSchedulerConstants;
import com.dd.domain.UploadDetail;
import com.dd.service.DDSchedulerService;
import com.dd.service.impl.DefaultDDSchedulerService;

@WebServlet(description = "Upload File To The Server", urlPatterns = { "/fileUploadServlet" })
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, maxFileSize = 1024 * 1024 * 30, maxRequestSize = 1024 * 1024
		* 50)
public class FileUploadServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}

	public void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/** Get The Absolute Path Of The Web Application **/
		String applicationPath = getServletContext().getRealPath(""),
				uploadPath = applicationPath + File.separator + DDSchedulerConstants.UPLOAD_DIR;

		DDSchedulerService ddSchedulerService = new DefaultDDSchedulerService();

		File fileUploadDirectory = new File(uploadPath);

		if (!fileUploadDirectory.exists()) {
			fileUploadDirectory.mkdirs();
		}

		String fileName = "";
		UploadDetail details = null;
		List<UploadDetail> fileList = new ArrayList<UploadDetail>();
		ArrayList<String> activityList = new ArrayList<String>();
		ArrayList<String> scheduledList = new ArrayList<String>();

		for (Part part : request.getParts()) {
			fileName = extractFileName(part);

			details = new UploadDetail();
			details.setFileName(fileName);
			details.setFileSize(part.getSize() / DDSchedulerConstants.SIZE_LIMIT);
			try {
				part.write(uploadPath + File.separator + fileName);
				InputStream fileContent = part.getInputStream();
				String line1 = null;
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(fileContent, StandardCharsets.UTF_8));
				while ((line1 = bufferedReader.readLine()) != null) {
					String activityNameDuration = line1;
					activityList.add(activityNameDuration);
				}
				details.setUploadStatus(DDSchedulerConstants.FILE_UPLOAD_SUCCESS);
			} catch (IOException ioObj) {
				details.setUploadStatus(DDSchedulerConstants.FILE_UPLOAD_FAILURE + ioObj.getMessage());
			}
			fileList.add(details);
		}

		scheduledList = ddSchedulerService.prepareTeamSchedule(activityList);

		request.setAttribute("activityList", scheduledList);
		request.setAttribute("uploadedFiles", fileList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/fileuploadResponse.jsp");
		dispatcher.forward(request, response);
	}

	/***** Helper Method - This Method Is Used To Read The File Names *****/
	private String extractFileName(Part part) {
		String fileName = "", contentDisposition = part.getHeader("content-disposition");
		String[] items = contentDisposition.split(";");
		for (String item : items) {
			if (item.trim().startsWith("filename")) {
				fileName = item.substring(item.indexOf("=") + 2, item.length() - 1);
			}
		}
		return fileName;
	}
}