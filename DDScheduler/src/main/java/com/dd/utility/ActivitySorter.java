package com.dd.utility;

import com.dd.domain.Activity;

import java.util.Comparator;

public class ActivitySorter implements Comparator<Activity>
{
	/*Comparator to sort in descending order*/
    public int compare(Activity a, Activity b)
    {
    	return (b.getDuration()-a.getDuration());
    }
}
