package com.dd.service;

import java.util.ArrayList;

import com.dd.domain.Activity;

public interface DDSchedulerService {

	ArrayList<Activity> extractDurationFromActivity(ArrayList<String> activityList);
	
    ArrayList<Activity> sortActivityOnDuration(ArrayList<Activity> activityList);
	
    ArrayList<Activity> getMorningSchedule(ArrayList<Activity> activityList);
	
    ArrayList<Activity> getNoonSchedule(ArrayList<Activity> activityList);
    
    String getTeamInfo(int teamNumber);
	
	ArrayList<String> prepareTeamSchedule(ArrayList<String> activityList);
	
	Boolean checkAllScheduled(ArrayList<Activity> activityList);
	
	String getActivityInfo(String time,String activityName,String activityLength);
	
	int getDurationInMin(String durationString);
	
	String getDurationString(int duration);
	
}
