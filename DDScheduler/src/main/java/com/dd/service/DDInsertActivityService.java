package com.dd.service;

import java.util.ArrayList;

import com.dd.domain.Activity;

public interface DDInsertActivityService {

	ArrayList<Activity> insertActivity(ArrayList<Activity> activityList);
}
