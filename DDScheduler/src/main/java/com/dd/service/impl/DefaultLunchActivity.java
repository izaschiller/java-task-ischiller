package com.dd.service.impl;

import java.util.ArrayList;

import com.dd.constants.DDSchedulerConstants;
import com.dd.domain.Activity;
import com.dd.service.DDInsertActivityService;

public class DefaultLunchActivity implements DDInsertActivityService{

	public ArrayList<Activity> insertActivity(ArrayList<Activity> activityList) {
		Activity a = new Activity();
		a.setActivityName(DDSchedulerConstants.LUNCH_BREAK);
		a.setDuration(60);
		activityList.add(a);
		return activityList;
	}
}
