package com.dd.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import org.apache.commons.collections4.CollectionUtils;

import com.dd.constants.DDSchedulerConstants;
import com.dd.domain.Activity;
import com.dd.service.DDInsertActivityService;
import com.dd.service.DDSchedulerService;
import com.dd.utility.ActivitySorter;

public class DefaultDDSchedulerService implements DDSchedulerService {

	/**
	 * Method to separate activity duration string from activity name and store
	 * separately in Activity object
	 **/

	public ArrayList<Activity> extractDurationFromActivity(ArrayList<String> activityList) {
		ArrayList<Activity> activityObjectList = new ArrayList<Activity>();
		for (String activityDuration : activityList) {
			Activity a = new Activity();
			int durationMin = DDSchedulerConstants.INT_ZERO;
			/* Extract last word of the string as the duration */
			String durationString = activityDuration
					.substring(activityDuration.lastIndexOf(" ") + DDSchedulerConstants.INT_ONE);
			/*
			 * Extract substring from beginning of string till last blank space as the
			 * activity name
			 */
			String activityName = activityDuration.substring(DDSchedulerConstants.INT_ZERO,
					activityDuration.lastIndexOf(" "));
			a.setActivityName(activityName);
			durationMin = getDurationInMin(durationString);
			a.setDuration(durationMin);
			activityObjectList.add(a);
		}
		return activityObjectList;
	}

	/** Method to convert duration string appended with min string to integer **/
	public int getDurationInMin(String durationString) {
		int sprint = DDSchedulerConstants.INT_SPRINT_DURATION;
		if (durationString.equalsIgnoreCase(DDSchedulerConstants.SPRINT)) {
			return sprint;
		} else {
			String duration = durationString.replaceAll(DDSchedulerConstants.MIN, "");
			/* Convert duration string to integer */
			int min = Integer.parseInt(duration);
			return min;
		}
	}

	/* Sort the activity list in descending order of duration */
	public ArrayList<Activity> sortActivityOnDuration(ArrayList<Activity> activityList) {
		Collections.sort(activityList, new ActivitySorter());
		return activityList;
	}

	/* Schedule morning slots from the activity list */
	public ArrayList<Activity> getMorningSchedule(ArrayList<Activity> activityList) {

		ArrayList<Activity> morningList = new ArrayList<Activity>();
		int sum = DDSchedulerConstants.INT_ZERO;
		int activityListLength = activityList.size();
		for (int i = 0; i < activityListLength; i++) {
			int updatedSum = sum + activityList.get(i).getDuration();
			Boolean isActivityAdded = activityList.get(i).isAdded();
			if ((updatedSum <= DDSchedulerConstants.MAX_MORN_LIMIT) && !isActivityAdded) {
				Activity a = new Activity();
				sum = sum + activityList.get(i).getDuration();
				a.setActivityName(activityList.get(i).getActivityName());
				a.setDuration(activityList.get(i).getDuration());
				morningList.add(a);
				activityList.get(i).setAdded(Boolean.TRUE);
			}
		}
		return morningList;
	}

	/* Schedule noon slots from the activity list */
	public ArrayList<Activity> getNoonSchedule(ArrayList<Activity> activityList) {
		ArrayList<Activity> noonList = new ArrayList<Activity>();
		int sum = DDSchedulerConstants.INT_ZERO;
		Boolean allScheduled = Boolean.FALSE;
		int activityListLength = activityList.size();

		while (sum < DDSchedulerConstants.MIN_NOON_LIMIT && !allScheduled) {
			for (int i = 0; i < activityListLength; i++) {
				int updatedSum = sum + activityList.get(i).getDuration();
				Boolean isActivityAdded = activityList.get(i).isAdded();
				if ((updatedSum <= DDSchedulerConstants.MAX_NOON_LIMIT) && !isActivityAdded) {
					Activity a = new Activity();
					sum = sum + activityList.get(i).getDuration();
					a.setActivityName(activityList.get(i).getActivityName());
					a.setDuration(activityList.get(i).getDuration());
					noonList.add(a);
					activityList.get(i).setAdded(Boolean.TRUE);
				}
			}
			allScheduled = checkAllScheduled(activityList);
		}
		return noonList;
	}

	/* Prepare the morning schedule and noon schedule for multiple teams */
	public ArrayList<String> prepareTeamSchedule(ArrayList<String> activityList) {

		ArrayList<String> schedule = new ArrayList<String>();
		ArrayList<Activity> morningList = new ArrayList<Activity>();
		ArrayList<Activity> noonList = new ArrayList<Activity>();
		String dateString = DDSchedulerConstants.STANDARD_START_TIME;
		SimpleDateFormat sdf = new SimpleDateFormat(DDSchedulerConstants.DATE_FORMAT);
		Boolean allScheduled = Boolean.FALSE;
		int teamNumber = DDSchedulerConstants.INT_ONE;

		ArrayList<Activity> activityObjectList = extractDurationFromActivity(activityList);
		activityObjectList = sortActivityOnDuration(activityObjectList);
		try {
			Date startTime = sdf.parse(dateString);
			while (!allScheduled) {

				String teamInfo = "";
				int min = DDSchedulerConstants.INT_ZERO;
				teamInfo = getTeamInfo(teamNumber);
				schedule.add(teamInfo);
				morningList = getMorningSchedule(activityObjectList);
				if (!allScheduled) {
					noonList = getNoonSchedule(activityObjectList);
				}
				if (CollectionUtils.isNotEmpty(noonList)) {
					DDInsertActivityService ddInsertLunchActivity = new DefaultLunchActivity();
					morningList = ddInsertLunchActivity.insertActivity(morningList);
					morningList.addAll(noonList);
					DDInsertActivityService ddInsertPresentationActivity = new DefaultPresentationActivity();
					morningList = ddInsertPresentationActivity.insertActivity(morningList);
				}
				for (Activity a : morningList) {
					Date d = Date.from(startTime.toInstant().plus(Duration.ofMinutes(Long.valueOf(min))));
					String time = sdf.format(d);
					String activityName = a.getActivityName();
					int activityDuration = a.getDuration();
					String activityLength = getDurationString(activityDuration);
					String activityInfo = getActivityInfo(time, activityName, activityLength);
					schedule.add(activityInfo);
					min = min + a.getDuration();
				}
				allScheduled = checkAllScheduled(activityObjectList);
				++teamNumber;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		/** Code for printing the result on the console starts **/
		for (String s : schedule) {
			System.out.println(s);
		}
		/** Code for printing the result on the console ends **/
		return schedule;
	}
    /**Check if all the activities in the given list has been scheduled**/
	public Boolean checkAllScheduled(ArrayList<Activity> activityList) {
		Boolean allScheduled = Boolean.FALSE;
		for (Activity a : activityList) {
			if (a.isAdded()) {
				allScheduled = Boolean.TRUE;
			} else {
				allScheduled = Boolean.FALSE;
				break;
			}
		}
		return allScheduled;
	}
    
	public String getTeamInfo(int teamNumber) {
		String teamInfo = "";
		teamInfo = DDSchedulerConstants.TEAM + " " + String.valueOf(teamNumber) + DDSchedulerConstants.COLON;
		return teamInfo;
	}

	public String getDurationString(int duration) {
		String textDuration = "";
		if (duration != 0) {
			textDuration = String.valueOf(duration) + DDSchedulerConstants.MIN;
		}
		return textDuration;
	}

	public String getActivityInfo(String time, String activityName, String activityLength) {
		String formattedTime = time.toLowerCase();
		String formattedActivityInfo = formattedTime + " " + DDSchedulerConstants.COLON + " " + activityName + " "
				+ activityLength;
		return formattedActivityInfo;
	}

}
