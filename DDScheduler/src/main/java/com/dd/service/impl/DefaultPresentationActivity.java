package com.dd.service.impl;

import java.util.ArrayList;

import com.dd.constants.DDSchedulerConstants;
import com.dd.domain.Activity;
import com.dd.service.DDInsertActivityService;

public class DefaultPresentationActivity implements DDInsertActivityService{

	public ArrayList<Activity> insertActivity(ArrayList<Activity> activityList) {
		Activity a = new Activity();
		a.setActivityName(DDSchedulerConstants.MOVITATION_PRESENTATION);
		a.setDuration(0);
		activityList.add(a);
		return activityList;
	}
}
