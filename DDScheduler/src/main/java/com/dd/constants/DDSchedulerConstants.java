package com.dd.constants;

public class DDSchedulerConstants {

	public static final int INT_ZERO=0;
	public static final int INT_ONE=1;
	public static final int INT_SPRINT_DURATION=15;
	public static final String SPRINT="sprint";
	public static final String MIN="min";
	public static final int MAX_MORN_LIMIT=180;
	public static final int MIN_NOON_LIMIT=180;
	public static final int MAX_NOON_LIMIT=240;
	public static final String STANDARD_START_TIME="09.00 AM";
	public static final String TEAM="Team";
	public static final String COLON=":";
	public static final String DATE_FORMAT="hh.mm aa";
	public static final String LUNCH_BREAK="Lunch Break";
	public static final String MOVITATION_PRESENTATION="Staff Motivation Presentation";
	public static final String UPLOAD_DIR = "uploadedFiles";
	public static final int SIZE_LIMIT = 1024;
	public static final String FILE_UPLOAD_SUCCESS= "Success";
	public static final String FILE_UPLOAD_FAILURE= "Failure";
}
