package com.dd.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import static org.hamcrest.Matchers.is;

import com.dd.constants.DDSchedulerConstants;
import com.dd.domain.Activity;

public class DefaultDDSchedulerServiceTest {

	@InjectMocks
	DefaultDDSchedulerService defaultDDSchedulerService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void extractDurationFromActivityTest() {
		ArrayList<String> activityList = new ArrayList<String>();
		String activityOne = "Solving puzzles 45min";
		String activityTwo = "Duck herding 30min";
		String activityThree = "Singing songs 15min";
		activityList.add(activityOne);
		activityList.add(activityTwo);
		activityList.add(activityThree);

		ArrayList<Activity> expectedList = new ArrayList<Activity>();
		Activity aOne = new Activity();
		aOne.setActivityName("Solving puzzles");
		aOne.setDuration(45);
		Activity aTwo = new Activity();
		aTwo.setActivityName("Duck herding");
		aTwo.setDuration(30);
		Activity aThree = new Activity();
		aThree.setActivityName("Singing songs");
		aThree.setDuration(15);

		expectedList.add(aOne);
		expectedList.add(aTwo);
		expectedList.add(aThree);

		ArrayList<Activity> resultList = new ArrayList<Activity>();

		resultList = defaultDDSchedulerService.extractDurationFromActivity(activityList);
		
		assertNotNull(resultList);

		assertEquals(expectedList.get(0).getActivityName(), resultList.get(0).getActivityName());
		assertEquals(expectedList.get(0).getDuration(), resultList.get(0).getDuration());

		assertEquals(expectedList.get(1).getActivityName(), resultList.get(1).getActivityName());
		assertEquals(expectedList.get(1).getDuration(), resultList.get(1).getDuration());

		assertEquals(expectedList.get(2).getActivityName(), resultList.get(2).getActivityName());
		assertEquals(expectedList.get(2).getDuration(), resultList.get(2).getDuration());
	}

	@Test
	public void getDurationInMinTest() {
		String durationString = "150min";
		int duration = defaultDDSchedulerService.getDurationInMin(durationString);
		int expectedDuration = 150;
		assertEquals(expectedDuration, duration);
	}

	@Test
	public void sortActivityOnDurationTest() {
		ArrayList<Activity> activityList = new ArrayList<Activity>();
		Activity aOne = new Activity();
		aOne.setActivityName("Solving puzzles");
		aOne.setDuration(15);
		Activity aTwo = new Activity();
		aTwo.setActivityName("Duck herding");
		aTwo.setDuration(45);
		Activity aThree = new Activity();
		aThree.setActivityName("Singing songs");
		aThree.setDuration(30);

		activityList.add(aOne);
		activityList.add(aTwo);
		activityList.add(aThree);

		ArrayList<Activity> expectedList = new ArrayList<Activity>();
		Activity aFour = new Activity();
		aFour.setActivityName("Duck herding");
		aFour.setDuration(45);
		Activity aFive = new Activity();
		aFive.setActivityName("Singing songs");
		aFive.setDuration(30);
		Activity aSix = new Activity();
		aSix.setActivityName("Solving puzzles");
		aSix.setDuration(15);

		expectedList.add(aFour);
		expectedList.add(aFive);
		expectedList.add(aSix);

		ArrayList<Activity> resultList = defaultDDSchedulerService.sortActivityOnDuration(activityList);
		
		assertNotNull(resultList);

		assertEquals(expectedList.get(0).getActivityName(), resultList.get(0).getActivityName());
		assertEquals(expectedList.get(0).getDuration(), resultList.get(0).getDuration());

		assertEquals(expectedList.get(1).getActivityName(), resultList.get(1).getActivityName());
		assertEquals(expectedList.get(1).getDuration(), resultList.get(1).getDuration());

		assertEquals(expectedList.get(2).getActivityName(), resultList.get(2).getActivityName());
		assertEquals(expectedList.get(2).getDuration(), resultList.get(2).getDuration());

	}

	@Test
	public void getMorningScheduleTest() {
		ArrayList<Activity> activityList = new ArrayList<Activity>();
		Activity aOne = new Activity();
		aOne.setActivityName("Solving puzzles");
		aOne.setDuration(60);
		Activity aTwo = new Activity();
		aTwo.setActivityName("Duck herding");
		aTwo.setDuration(60);
		Activity aThree = new Activity();
		aThree.setActivityName("Singing songs");
		aThree.setDuration(45);
		Activity aFour = new Activity();
		aFour.setActivityName("Archery");
		aFour.setDuration(60);

		activityList.add(aOne);
		activityList.add(aTwo);
		activityList.add(aThree);
		activityList.add(aFour);

		ArrayList<Activity> expectedList = new ArrayList<Activity>();
		Activity aFive = new Activity();
		aFive.setActivityName("Solving puzzles");
		aFive.setDuration(60);
		Activity aSix = new Activity();
		aSix.setActivityName("Duck herding");
		aSix.setDuration(60);
		Activity aSeven = new Activity();
		aSeven.setActivityName("Singing songs");
		aSeven.setDuration(45);

		expectedList.add(aFive);
		expectedList.add(aSix);
		expectedList.add(aSeven);

		ArrayList<Activity> resultList = defaultDDSchedulerService.getMorningSchedule(activityList);
		
		assertNotNull(resultList);

		assertEquals(resultList.size(), 3);

		assertEquals(expectedList.get(0).getActivityName(), resultList.get(0).getActivityName());
		assertEquals(expectedList.get(0).getDuration(), resultList.get(0).getDuration());

		assertEquals(expectedList.get(1).getActivityName(), resultList.get(1).getActivityName());
		assertEquals(expectedList.get(1).getDuration(), resultList.get(1).getDuration());

		assertEquals(expectedList.get(2).getActivityName(), resultList.get(2).getActivityName());
		assertEquals(expectedList.get(2).getDuration(), resultList.get(2).getDuration());

	}

	@Test
	public void getNoonScheduleTest() {
		ArrayList<Activity> activityList = new ArrayList<Activity>();
		Activity aOne = new Activity();
		aOne.setActivityName("Solving puzzles");
		aOne.setDuration(60);
		Activity aTwo = new Activity();
		aTwo.setActivityName("Duck herding");
		aTwo.setDuration(60);
		Activity aThree = new Activity();
		aThree.setActivityName("Singing songs");
		aThree.setDuration(45);
		Activity aFour = new Activity();
		aFour.setActivityName("Archery");
		aFour.setDuration(60);
		Activity aFive = new Activity();
		aFive.setActivityName("Throw ball");
		aFive.setDuration(45);

		activityList.add(aOne);
		activityList.add(aTwo);
		activityList.add(aThree);
		activityList.add(aFour);
		activityList.add(aFive);

		ArrayList<Activity> expectedList = new ArrayList<Activity>();
		Activity aSix = new Activity();
		aSix.setActivityName("Solving puzzles");
		aSix.setDuration(60);
		Activity aSeven = new Activity();
		aSeven.setActivityName("Duck herding");
		aSeven.setDuration(60);
		Activity aEight = new Activity();
		aEight.setActivityName("Singing songs");
		aEight.setDuration(45);
		Activity aNine = new Activity();
		aNine.setActivityName("Archery");
		aNine.setDuration(60);

		expectedList.add(aSix);
		expectedList.add(aSeven);
		expectedList.add(aEight);
		expectedList.add(aNine);

		ArrayList<Activity> resultList = defaultDDSchedulerService.getNoonSchedule(activityList);
		
		assertNotNull(resultList);

		assertEquals(resultList.size(), 4);

		assertEquals(expectedList.get(0).getActivityName(), resultList.get(0).getActivityName());
		assertEquals(expectedList.get(0).getDuration(), resultList.get(0).getDuration());

		assertEquals(expectedList.get(1).getActivityName(), resultList.get(1).getActivityName());
		assertEquals(expectedList.get(1).getDuration(), resultList.get(1).getDuration());

		assertEquals(expectedList.get(2).getActivityName(), resultList.get(2).getActivityName());
		assertEquals(expectedList.get(2).getDuration(), resultList.get(2).getDuration());

		assertEquals(expectedList.get(3).getActivityName(), resultList.get(3).getActivityName());
		assertEquals(expectedList.get(3).getDuration(), resultList.get(3).getDuration());

	}

	@Test
	public void prepareTeamScheduleTest() {

		ArrayList<String> activityList = new ArrayList<String>();
		String aOne = "Solving puzzles 60min";
		String aTwo = "Duck herding 60min";
		String aThree = "Archery 50min";
		String aFour = "Chess 45min";
		String aFive = "Ludo 60min";
		String aSix = "Carroms 40min";
		String aSeven = "Duck herding 30min";
		String aEight = "Go Cart Racing 30min";
		String aNine = "Tree planting 30min";

		activityList.add(aOne);
		activityList.add(aTwo);
		activityList.add(aThree);
		activityList.add(aFour);
		activityList.add(aFive);
		activityList.add(aSix);
		activityList.add(aSeven);
		activityList.add(aEight);
		activityList.add(aNine);

		ArrayList<String> resultList = defaultDDSchedulerService.prepareTeamSchedule(activityList);
		
		assertNotNull(resultList);

		ArrayList<String> expectedList = new ArrayList<String>();
		String aTwentyOne = "Team 1:";
		String aTen = "09.00 am : Solving puzzles 60min";
		String aEleven = "10.00 am : Duck herding 60min";
		String aTwelve = "11.00 am : Ludo 60min";
		String aThirteen = "12.00 pm : Lunch Break 60min";
		String aFourteen = "01.00 pm : Archery 50min";
		String aFifteen = "01.50 pm : Chess 45min";
		String aSixteen = "02.35 pm : Carroms 40min";
		String aSeventeen = "03.15 pm : Duck herding 30min";
		String aEighteen = "03.45 pm : Go Cart Racing 30min";
		String aNineteen = "04.15 pm : Tree planting 30min";
		String aTwenty = "04.45 pm : Staff Motivation Presentation";

		expectedList.add(aTwentyOne);
		expectedList.add(aTen);
		expectedList.add(aEleven);
		expectedList.add(aTwelve);
		expectedList.add(aThirteen);
		expectedList.add(aFourteen);
		expectedList.add(aFifteen);
		expectedList.add(aSixteen);
		expectedList.add(aSeventeen);
		expectedList.add(aEighteen);
		expectedList.add(aNineteen);
		expectedList.add(aTwenty);

		assertEquals(expectedList.get(0), resultList.get(0));
		assertEquals(expectedList.get(1), resultList.get(1));
		assertEquals(expectedList.get(2), resultList.get(2));
		assertEquals(expectedList.get(3), resultList.get(3));
		assertEquals(expectedList.get(4), resultList.get(4));
		assertEquals(expectedList.get(5), resultList.get(5));
		assertEquals(expectedList.get(6), resultList.get(6));
		assertEquals(expectedList.get(7), resultList.get(7));
		assertEquals(expectedList.get(8), resultList.get(8));
		assertEquals(expectedList.get(9), resultList.get(9));
		assertEquals(expectedList.get(10), resultList.get(10));
		
	}

	@Test
	public void checkAllScheduledTest() {
		ArrayList<Activity> activityList = new ArrayList<Activity>();
		Activity aOne = new Activity();
		aOne.setActivityName("Solving puzzles");
		aOne.setDuration(15);
		aOne.setAdded(Boolean.TRUE);
		Activity aTwo = new Activity();
		aTwo.setActivityName("Duck herding");
		aTwo.setDuration(45);
		aTwo.setAdded(Boolean.TRUE);
		Activity aThree = new Activity();
		aThree.setActivityName("Singing songs");
		aThree.setDuration(30);
		aThree.setAdded(Boolean.TRUE);

		activityList.add(aOne);
		activityList.add(aTwo);
		activityList.add(aThree);

		Boolean allScheduled = defaultDDSchedulerService.checkAllScheduled(activityList);

		assertEquals(Boolean.TRUE, allScheduled);
	}

	@Test
	public void getTeamInfoTest() {
		int teamNumber = 89;
		String expectedTeamInfo = "Team 89:";
		String resultTeamInfo = defaultDDSchedulerService.getTeamInfo(teamNumber);
		
		assertEquals(expectedTeamInfo, resultTeamInfo);
	}

	@Test
	public void getDurationStringTest() {
		int duration = 60;
		String expectedDuration = "60min";
		String resultDuration = defaultDDSchedulerService.getDurationString(duration);
		
		assertEquals(expectedDuration, resultDuration);
	}

	@Test
	public void getActivityInfoTest() {
		String time = "9:00 am";
		String activityName = "Duck Herding";
		String activityLength = "40min";
		String expectedActivityInfo = "9:00 am : Duck Herding 40min";

		String resultActivityInfo = defaultDDSchedulerService.getActivityInfo(time, activityName, activityLength);
		assertEquals(expectedActivityInfo, resultActivityInfo);
	}

}
