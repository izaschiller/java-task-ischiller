package com.dd.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.dd.domain.Activity;
import com.dd.service.DDInsertActivityService;

public class DefaultPresentationActivityTest {

	@InjectMocks
	DDInsertActivityService defaultPresentationActivity=new DefaultPresentationActivity();;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void insertActivityTest() {
		ArrayList<Activity> activityList=new ArrayList<Activity>();
		Activity aOne=new Activity();
		aOne.setActivityName("Duck herding");
		aOne.setDuration(45);
		activityList.add(aOne);
		
		ArrayList<Activity> expectedList=new ArrayList<Activity>();
		Activity aTwo=new Activity();
		aTwo.setActivityName("Duck herding");
		aTwo.setDuration(45);
		expectedList.add(aTwo);
		Activity aThree=new Activity();
		aThree.setActivityName("Staff Motivation Presentation");
		aThree.setDuration(60);
		expectedList.add(aThree);
		
		ArrayList<Activity> resultList=defaultPresentationActivity.insertActivity(activityList);
		assertNotNull(resultList);
		assertEquals(expectedList.get(0).getActivityName(), resultList.get(0).getActivityName());
        assertEquals(expectedList.get(0).getDuration(), resultList.get(0).getDuration());   
        assertEquals(expectedList.get(1).getActivityName(), resultList.get(1).getActivityName());
        assertEquals(resultList.get(1).getDuration(),0);
        
	}

}
