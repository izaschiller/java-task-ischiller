package com.dd.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.dd.domain.Activity;
import com.dd.service.DDInsertActivityService;

public class DefaultLunchActivityTest {

	@InjectMocks
	DDInsertActivityService defaultLunchActivity = new DefaultLunchActivity();;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void insertActivityTest() {
		ArrayList<Activity> activityList = new ArrayList<Activity>();
		Activity aOne = new Activity();
		aOne.setActivityName("Duck herding");
		aOne.setDuration(45);
		activityList.add(aOne);

		ArrayList<Activity> expectedList = new ArrayList<Activity>();
		Activity aTwo = new Activity();
		aTwo.setActivityName("Duck herding");
		aTwo.setDuration(45);
		expectedList.add(aTwo);
		Activity aThree = new Activity();
		aThree.setActivityName("Lunch Break");
		aThree.setDuration(60);
		expectedList.add(aThree);

		ArrayList<Activity> resultList = defaultLunchActivity.insertActivity(activityList);
		assertNotNull(resultList);
		assertEquals(expectedList.get(0).getActivityName(), resultList.get(0).getActivityName());
		assertEquals(expectedList.get(0).getDuration(), resultList.get(0).getDuration());
		assertEquals(expectedList.get(1).getActivityName(), resultList.get(1).getActivityName());
		assertEquals(expectedList.get(1).getDuration(), resultList.get(1).getDuration());
	}
}
