package com.dd.utility;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;


import com.dd.domain.Activity;

public class ActivitySorterTest {

	@InjectMocks
	ActivitySorter activitySorter;
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	@Test
	public void compareTest() {
		Activity a=new Activity();
		Activity b=new Activity();
		a.setDuration(10);
		a.setDuration(20);
		int result=activitySorter.compare(a,b);
		assertTrue(result<0);
	}
}
